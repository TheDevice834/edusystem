<?php
namespace app\controllers;

use system\Controller;
use app\models\Feed;
use app\models\User;
use app\models\Teacher;

class Teachers extends Controller {
	public function list(){
        check_is_auth();

		$teachers = Teacher::findAll('ORDER BY id DESC');

		$this->view->render('teachers/list.html.twig', [
			'title' => 'Преподаватели',
			'teachers' => $teachers
		]);
	}

	public function edit(){
		check_is_auth();

		$teacher = Teacher::load($this->request['GET']['id']);
		if(!$teacher->id) return not_found();

		if($this->request['METHOD'] == 'POST'){
			$t_title = $this->request['POST']['t_title'];

			if(!Teacher::checkTeacherTitle($t_title) && $t_title != $teacher['title']){
				add_alert('danger', 'Имя преподавателя <b>'.$t_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Teacher::findOne('title = ?', [$t_title])) && $t_title != $teacher['title']){
				add_alert('danger', 'Преподаватель с именем <b>'.$t_title.'</b> уже существует.');
				return redirect('', false);
			}

			if($t_title != $teacher['title']){
				$old_t_title = $teacher['title'];
				$teacher['title'] = $t_title;

				$teacher = Teacher::store($teacher);

				$feed = Feed::toWrapUp(
					$_SESSION['user']['id'],
					11,
					[
						'author_id' => $_SESSION['user']['id'],
						'author_login' => $_SESSION['user']['login'],
						'old_t_title' => $old_t_title,
						't_title' => $teacher['title']
					]
				);

				Feed::add($feed);
			}

			add_alert('success', 'Преподаватель <b>'.$s_title.'</b> сохранен.');
			return redirect('', false);
		} else {
			$this->view->render('teachers/add.html.twig', [
				'title' => 'Изменение имени преподавателя '.$teacher['title'],
				'teacher' => $teacher,
				'type' => 'edit'
			]);
		}
	}

	public function add(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$t_title = $this->request['POST']['t_title'];

			if(!Teacher::checkTeacherTitle($t_title)){
				add_alert('danger', 'Имя преподавателя <b>'.$t_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Teacher::findOne('title = ?', [$t_title]))){
				add_alert('danger', 'Преподаватель с именем <b>'.$t_title.'</b> уже существует.');
				return redirect('', false);
			}

			$teacher = Teacher::add([
				'title' => $t_title,
			]);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				10,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					't_title' => $teacher['title']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Преподаватель <b>'.$t_title.'</b> добавлен.');
			return redirect(generateUrl(['teachers', 'list']));
		} else {
			$this->view->render('teachers/add.html.twig', [
				'title' => 'Добавить преподавателя',
				'type' => 'add'
			]);
		}
	}

	public function delete(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$teacher = Teacher::load($this->request['GET']['id']);
			if(!$teacher->id) ajax([
				'type' => 'error',
				'message' => 'Преподаватель не найден.'
			]);

			Teacher::trash($teacher);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				12,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					't_title' => $teacher['title']
				]
			);

			Feed::add($feed);

			ajax([
				'type' => 'success',
				'message' => 'Преподаватель <b>'.$teacher['title'].'</b> удален.'
			]);
		}
	}
}
