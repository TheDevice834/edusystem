<?php
namespace app\controllers;

use system\Controller;
use app\models\User;

class Auth extends Controller {
	public function signin(){
		check_is_not_auth();

		if($this->request['METHOD'] == 'POST'){
			$login = $this->request['POST']['login'];
			$password = $this->request['POST']['password'];

			if($login == '' || $password == ''){
				ajax([
					'type' => 'error',
					'message' => 'Заполните все поля.'
				]);
			}

			if(empty($user = User::findOne('login = ? AND password = ?', [
				$login, md5($password)
			]))){
				ajax([
					'type' => 'error',
					'message' => 'Неверный логин или пароль.'
				]);
			}

			$_SESSION['user'] = [
				'id' => $user['id'],
				'login' => $user['login']
			];

			if(isset($this->request['GET']['back'])) $next_url = $this->request['GET']['back'];
			else $next_url = '/';

			ajax([
				'type' => 'success',
				'message' => 'Вы успешно вошли.<br>Вы будете перенаправлены через 3 секунды.',
				'next_url' => $next_url
			]);
		} else {
			$this->view->render('auth/signin.html.twig', ['title' => 'Вход']);
		}
	}

	public function signout(){
		check_is_auth();

		if(isset($_SESSION['user'])) unset($_SESSION['user']);

		return redirect();
	}
}
