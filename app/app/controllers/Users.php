<?php
namespace app\controllers;

use system\Controller;
use app\models\User;
use app\models\Feed;

class Users extends Controller {
	public function list(){
		check_is_auth();

		$users = User::findAll('ORDER BY id DESC');

		$this->view->render('users/list.html.twig', ['title' => 'Модераторы', 'users' => $users]);
	}

	public function view(){
		check_is_auth();

		$user = User::load($this->request['GET']['id']);
		if(!$user->id) return not_found();

		$this->view->render('users/view.html.twig', [
			'title' => 'Профиль модератора '.$user['login'],
			'user' => $user
		]);
	}

	public function edit(){
		check_is_auth();

		$user = User::load($this->request['GET']['id']);
		if(!$user->id) return not_found();

		if($this->request['METHOD'] == 'POST'){
			$u_login = $this->request['POST']['u_login'];
			$u_password = $this->request['POST']['u_password'];

			if(!User::checkLogin($u_login) && $u_login != $user['login']){
				add_alert('danger', 'Логин <b>'.$u_login.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!User::checkPassword($u_password) && $u_password != ''){
				add_alert('danger', 'Пароль <b>'.$u_password.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(User::findOne('login = ?', [$u_login])) && $u_login != $user['login']){
				add_alert('danger', 'Модератор с логином <b>'.$u_login.'</b> уже существует.');
				return redirect('', false);
			}

			if($u_login != $user['login']) $user['login'] = $u_login;
			if(md5($u_password) != $user['password'] && $u_password != '') $user['password'] = md5($u_password);

			$user = User::store($user);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				3,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'moderator_id' => $user['id'],
					'moderator_login' => $user['login']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Модератор <b>'.$u_login.' (ID: '.$user['id'].')</b> сохранен.');
			return redirect('', false);
		} else {
			$this->view->render('users/add.html.twig', [
				'title' => 'Редактирование модератора '.$user['login'],
				'user' => $user,
				'type' => 'edit'
			]);
		}
	}

	public function add(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$u_login = $this->request['POST']['u_login'];
			$u_password = $this->request['POST']['u_password'];

			if(!User::checkLogin($u_login)){
				add_alert('danger', 'Логин <b>'.$u_login.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!User::checkPassword($u_password)){
				add_alert('danger', 'Пароль <b>'.$u_password.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(User::findOne('login = ?', [$u_login]))){
				add_alert('danger', 'Модератор с логином <b>'.$u_login.'</b> уже существует.');
				return redirect('', false);
			}

			$user = User::add([
				'login' => $u_login,
				'password' => md5($u_password),
				'regdate' => time()
			]);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				1,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'moderator_id' => $user['id'],
					'moderator_login' => $user['login']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Модератор <b>'.$u_login.' (ID: '.$user['id'].')</b> добавлен.');
			return redirect(generateUrl(['users', 'list']));
		} else {
			$this->view->render('users/add.html.twig', [
				'title' => 'Добавить модератора',
				'type' => 'add'
			]);
		}
	}

	public function delete(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$user = User::load($this->request['GET']['id']);
			if(!$user->id) ajax([
				'type' => 'error',
				'message' => 'Модератор не найден.'
			]);

			if($user['id'] == $_SESSION['user']['id']) ajax([
				'type' => 'error',
				'message' => 'Вы не можете удалить свой аккаунт.'
			]);

			User::trash($user);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				2,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'moderator_login' => $user['login']
				]
			);

			Feed::add($feed);

			ajax([
				'type' => 'success',
				'message' => 'Модератор <b>'.$user['login'].'</b> удален.'
			]);
		}
	}
}
