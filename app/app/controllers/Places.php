<?php
namespace app\controllers;

use system\Controller;
use app\models\Feed;
use app\models\User;
use app\models\Place;

class Places extends Controller {
	public function list(){
        check_is_auth();

		$places = Place::findAll('ORDER BY id DESC');

		$this->view->render('places/list.html.twig', [
			'title' => 'Предметы',
			'places' => $places
		]);
	}

	public function edit(){
		check_is_auth();

		$place = Place::load($this->request['GET']['id']);
		if(!$place->id) return not_found();

		if($this->request['METHOD'] == 'POST'){
			$p_title = $this->request['POST']['p_title'];

			if(!Place::checkPlaceTitle($p_title) && $p_title != $place['title']){
				add_alert('danger', 'Название аудитории <b>'.$p_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Place::findOne('title = ?', [$p_title])) && $p_title != $place['title']){
				add_alert('danger', 'Аудитория с названием <b>'.$p_title.'</b> уже существует.');
				return redirect('', false);
			}

			if($p_title != $place['title']){
				$old_p_title = $place['title'];
				$place['title'] = $p_title;

				$place = Place::store($place);

				$feed = Feed::toWrapUp(
					$_SESSION['user']['id'],
					14,
					[
						'author_id' => $_SESSION['user']['id'],
						'author_login' => $_SESSION['user']['login'],
						'old_p_title' => $old_p_title,
						'p_title' => $place['title']
					]
				);

				Feed::add($feed);
			}

			add_alert('success', 'Аудитория <b>'.$p_title.'</b> сохранена.');
			return redirect('', false);
		} else {
			$this->view->render('places/add.html.twig', [
				'title' => 'Изменение названия аудитории '.$place['title'],
				'place' => $place,
				'type' => 'edit'
			]);
		}
	}

	public function add(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$p_title = $this->request['POST']['p_title'];

			if(!Place::checkPlaceTitle($p_title)){
				add_alert('danger', 'Название аудитории <b>'.$p_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Place::findOne('title = ?', [$p_title]))){
				add_alert('danger', 'Аудитория с названием <b>'.$p_title.'</b> уже существует.');
				return redirect('', false);
			}

			$place = Place::add([
				'title' => $p_title,
			]);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				13,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'p_title' => $place['title']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Аудитория <b>'.$p_title.'</b> добавлена.');
			return redirect(generateUrl(['places', 'list']));
		} else {
			$this->view->render('places/add.html.twig', [
				'title' => 'Добавить аудиторию',
				'type' => 'add'
			]);
		}
	}

	public function delete(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$place = Place::load($this->request['GET']['id']);
			if(!$place->id) ajax([
				'type' => 'error',
				'message' => 'Аудитория не найдена.'
			]);

			Place::trash($place);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				15,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'p_title' => $place['title']
				]
			);

			Feed::add($feed);

			ajax([
				'type' => 'success',
				'message' => 'Аудитория <b>'.$place['title'].'</b> удалена.'
			]);
		}
	}
}
