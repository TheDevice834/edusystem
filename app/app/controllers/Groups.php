<?php
namespace app\controllers;

use system\Controller;
use app\models\Feed;
use app\models\User;
use app\models\Group;

class Groups extends Controller {
	public function list(){
        check_is_auth();
        
		$groups = Group::findAll('ORDER BY id DESC');

		$this->view->render('groups/list.html.twig', [
			'title' => 'Группы',
			'groups' => $groups
		]);
	}

	public function edit(){
		check_is_auth();

		$group = Group::load($this->request['GET']['id']);
		if(!$group->id) return not_found();

		if($this->request['METHOD'] == 'POST'){
			$g_title = $this->request['POST']['g_title'];

			if(!Group::checkGroupTitle($g_title) && $g_title != $group['title']){
				add_alert('danger', 'Название группы <b>'.$g_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Group::findOne('title = ?', [$g_title])) && $g_title != $group['title']){
				add_alert('danger', 'Группа с названием <b>'.$g_title.'</b> уже существует.');
				return redirect('', false);
			}

			if($g_title != $group['title']){
				$old_g_title = $group['title'];
				$group['title'] = $g_title;

				$group = Group::store($group);

				$feed = Feed::toWrapUp(
					$_SESSION['user']['id'],
					6,
					[
						'author_id' => $_SESSION['user']['id'],
						'author_login' => $_SESSION['user']['login'],
						'old_g_title' => $old_g_title,
						'g_title' => $group['title']
					]
				);

				Feed::add($feed);
			}

			add_alert('success', 'Группа <b>'.$g_title.'</b> сохранена.');
			return redirect('', false);
		} else {
			$this->view->render('groups/add.html.twig', [
				'title' => 'Изменение названия группы '.$group['title'],
				'group' => $group,
				'type' => 'edit'
			]);
		}
	}

	public function add(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$g_title = $this->request['POST']['g_title'];

			if(!Group::checkGroupTitle($g_title)){
				add_alert('danger', 'Название группы <b>'.$g_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Group::findOne('title = ?', [$g_title]))){
				add_alert('danger', 'Группа с названием <b>'.$g_title.'</b> уже существует.');
				return redirect('', false);
			}

			$group = Group::add([
				'title' => $g_title,
			]);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				4,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'g_title' => $group['title']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Группа <b>'.$g_title.'</b> добавлена.');
			return redirect(generateUrl(['groups', 'list']));
		} else {
			$this->view->render('groups/add.html.twig', [
				'title' => 'Добавить группу',
				'type' => 'add'
			]);
		}
	}

	public function delete(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$group = Group::load($this->request['GET']['id']);
			if(!$group->id) ajax([
				'type' => 'error',
				'message' => 'Группа не найдена.'
			]);

			Group::trash($group);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				5,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					'g_title' => $group['title']
				]
			);

			Feed::add($feed);

			ajax([
				'type' => 'success',
				'message' => 'Группа <b>'.$group['title'].'</b> удалена.'
			]);
		}
	}
}
