<?php
namespace app\controllers;

use system\Controller;
use app\models\Feed;
use app\models\User;
use app\models\Schedule as ScheduleModel;

use app\models\Group;
use app\models\Subject;
use app\models\Teacher;
use app\models\Place;

class Schedule extends Controller {
	public function view(){
        check_is_auth();

        $rows = ScheduleModel::findAll('');

		$schedule = ScheduleModel::sortRowsToPanelView($rows);
		$schedule_days = ScheduleModel::dayToView();

		$this->view->render('schedule/view.html.twig', [
			'title' => 'Расписание',
			'schedule' => $schedule,
			'schedule_days' => $schedule_days
		]);
	}

	public function add(){
        check_is_auth();

	    if($this->request['METHOD'] == 'POST'){
            $day = intval($this->request['POST']['day']);
            $time = intval($this->request['POST']['time']);
            $group = intval($this->request['POST']['group_id']);
            $subject = intval($this->request['POST']['subject_id']);
            $teacher = intval($this->request['POST']['teacher_id']);
            $place = intval($this->request['POST']['place_id']);

            $row = ScheduleModel::add([
                'author_id' => $_SESSION['user']['id'],
                'created_at' => time(),
                'day' => $day,
                'time' => $time,
                'group_id' => $group,
                'subject_id' => $subject,
                'teacher_id' => $teacher,
                'place_id' => $place,
            ]);

            $group_db = Group::load($row['group_id']);

            $row_title = ScheduleModel::dayToView($row['day']).', '.$row['time'].'-й урок, '.$group_db['title'].' группа';

            $feed = Feed::toWrapUp(
                $_SESSION['user']['id'],
                16,
                [
                    'author_id' => $_SESSION['user']['id'],
                    'author_login' => $_SESSION['user']['login'],
                    'row_title' => $row_title
                ]
            );

            Feed::add($feed);

            add_alert('success', 'Запись <b>'.$row_title.'</b> добавлена.');
            return redirect(generateUrl(['schedule', 'view']));
        } else {
	        $groups = Group::findAll('');
            $subjects = Subject::findAll('');
            $teachers = Teacher::findAll('');
            $places = Place::findAll('');

            $this->view->render('schedule/add.html.twig', [
                'title' => 'Добавить запись',
                'type' => 'add',
                'groups' => $groups,
                'subjects' => $subjects,
                'teachers' => $teachers,
                'places' => $places
            ]);
        }
    }

    public function edit(){
        check_is_auth();

        $row = ScheduleModel::load($this->request['GET']['id']);
        if(!$row->id) return not_found();

        if($this->request['METHOD'] == 'POST'){
            foreach($this->request['POST'] as $key => $value){
                if($row[$key] != $value) $row[$key] = $value;
            }

            $row = ScheduleModel::store($row);

            $group_db = Group::load($row['group_id']);

            $row_title = ScheduleModel::dayToView($row['day']).', '.$row['time'].'-й урок, '.$group_db['title'].' группа';

            $feed = Feed::toWrapUp(
                $_SESSION['user']['id'],
                18,
                [
                    'author_id' => $_SESSION['user']['id'],
                    'author_login' => $_SESSION['user']['login'],
                    'row_title' => $row_title
                ]
            );

            Feed::add($feed);

            add_alert('success', 'Запись <b>'.$row_title.'</b> сохранена.');
            return redirect('');
        } else {
            $groups = Group::findAll('');
            $subjects = Subject::findAll('');
            $teachers = Teacher::findAll('');
            $places = Place::findAll('');

            $this->view->render('schedule/add.html.twig', [
                'title' => 'Добавить запись',
                'type' => 'edit',
                'row' => $row,
                'groups' => $groups,
                'subjects' => $subjects,
                'teachers' => $teachers,
                'places' => $places
            ]);
        }
    }

    public function delete(){
        check_is_auth();

        if($this->request['METHOD'] == 'POST'){
            $row = ScheduleModel::load($this->request['GET']['id']);
            if(!$row->id) ajax([
                'type' => 'error',
                'message' => 'Запись не найдена.'
            ]);
            $group_db = Group::load($row['group_id']);

            ScheduleModel::trash($row);

            $row_title = ScheduleModel::dayToView($row['day']).', '.$row['time'].'-й урок, '.$group_db['title'].' группа';

            $feed = Feed::toWrapUp(
                $_SESSION['user']['id'],
                17,
                [
                    'author_id' => $_SESSION['user']['id'],
                    'author_login' => $_SESSION['user']['login'],
                    'row_title' => $row_title
                ]
            );

            Feed::add($feed);

            ajax([
                'type' => 'success',
                'message' => 'Запись <b>'.$row_title.'</b> удалена.'
            ]);
        }
    }
}
