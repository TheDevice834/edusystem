<?php
namespace app\controllers;

use system\Controller;
use app\models\Feed;
use app\models\User;
use app\models\Subject;

class Subjects extends Controller {
	public function list(){
        check_is_auth();

		$subjects = Subject::findAll('ORDER BY id DESC');

		$this->view->render('subjects/list.html.twig', [
			'title' => 'Предметы',
			'subjects' => $subjects
		]);
	}

	public function edit(){
		check_is_auth();

		$subject = Subject::load($this->request['GET']['id']);
		if(!$subject->id) return not_found();

		if($this->request['METHOD'] == 'POST'){
			$s_title = $this->request['POST']['s_title'];

			if(!Subject::checkSubjectTitle($s_title) && $s_title != $subject['title']){
				add_alert('danger', 'Название предмета <b>'.$s_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Subject::findOne('title = ?', [$s_title])) && $s_title != $subject['title']){
				add_alert('danger', 'Предмет с названием <b>'.$s_title.'</b> уже существует.');
				return redirect('', false);
			}

			if($s_title != $subject['title']){
				$old_s_title = $subject['title'];
				$subject['title'] = $s_title;

				$subject = Subject::store($subject);

				$feed = Feed::toWrapUp(
					$_SESSION['user']['id'],
					8,
					[
						'author_id' => $_SESSION['user']['id'],
						'author_login' => $_SESSION['user']['login'],
						'old_s_title' => $old_s_title,
						's_title' => $subject['title']
					]
				);

				Feed::add($feed);
			}

			add_alert('success', 'Предмет <b>'.$s_title.'</b> сохранен.');
			return redirect('', false);
		} else {
			$this->view->render('subjects/add.html.twig', [
				'title' => 'Изменение названия предмета '.$subject['title'],
				'subject' => $subject,
				'type' => 'edit'
			]);
		}
	}

	public function add(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$s_title = $this->request['POST']['s_title'];

			if(!Subject::checkSubjectTitle($s_title)){
				add_alert('danger', 'Название предмета <b>'.$s_title.'</b> не соответствует правилам.');
				return redirect('', false);
			}

			if(!empty(Subject::findOne('title = ?', [$s_title]))){
				add_alert('danger', 'Предмет с названием <b>'.$s_title.'</b> уже существует.');
				return redirect('', false);
			}

			$subject = Subject::add([
				'title' => $s_title,
			]);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				7,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					's_title' => $subject['title']
				]
			);

			Feed::add($feed);

			add_alert('success', 'Предмет <b>'.$s_title.'</b> добавлен.');
			return redirect(generateUrl(['subjects', 'list']));
		} else {
			$this->view->render('subjects/add.html.twig', [
				'title' => 'Добавить предмет',
				'type' => 'add'
			]);
		}
	}

	public function delete(){
		check_is_auth();

		if($this->request['METHOD'] == 'POST'){
			$subject = Subject::load($this->request['GET']['id']);
			if(!$subject->id) ajax([
				'type' => 'error',
				'message' => 'Предмет не найден.'
			]);

			Subject::trash($subject);

			$feed = Feed::toWrapUp(
				$_SESSION['user']['id'],
				9,
				[
					'author_id' => $_SESSION['user']['id'],
					'author_login' => $_SESSION['user']['login'],
					's_title' => $subject['title']
				]
			);

			Feed::add($feed);

			ajax([
				'type' => 'success',
				'message' => 'Предмет <b>'.$subject['title'].'</b> удален.'
			]);
		}
	}
}
