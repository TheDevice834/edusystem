<?php
namespace app\controllers;

use app\models\Group;
use app\models\Place;
use app\models\Subject;
use app\models\Teacher;
use system\Controller;
use app\models\Feed;
use app\models\Schedule;

class Main extends Controller {
	public function index(){
		$this->view->render('main/index.html.twig', ['title' => 'Замены']);
	}

	public function panel(){
		check_is_auth();

		if(isset($this->request['GET']['page'])) $page = intval($this->request['GET']['page']);
		else $page = 1;

		$limit = 15;
		$offset = ($page * $limit) - $limit;

		$db_feed = Feed::findAll('ORDER BY id DESC LIMIT ?,?', [$offset, $limit]);
		$feeds_count = Feed::count();

		$pagination_buttons_num = ceil($feeds_count / $limit);

		$feed = [];
		foreach($db_feed as $value){
			$feed[] = Feed::convertFeedToView($value);
		}

		$this->view->render('main/panel.html.twig', [
			'title' => 'Главная',
			'feed' => $feed,
			'paginate_pagesnum' => $pagination_buttons_num,
			'paginate_currentpage' => $page
		]);
	}

	public function schedule(){
	    $rows = Schedule::findAll('');

	    $schedule = Schedule::sortRowsToView($rows);
	    $schedule_days = Schedule::dayToView();

		$this->view->render('main/schedule.html.twig', [
		    'title' => 'Расписание',
            'schedule' => $schedule,
            'schedule_days' => $schedule_days
        ]);
	}
}
