<?php
namespace app\models;

use system\Model;

class Subject extends Model {
	static $table_name = 'subjects';

	public static function checkSubjectTitle($title){
		$max_length = 64;
		$min_length = 2;

		if(strlen($title) < $min_length || strlen($title) > $max_length) return false;

		return true;
	}
}
