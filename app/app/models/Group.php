<?php
namespace app\models;

use system\Model;

class Group extends Model {
	static $table_name = 'groups';

	public static function checkGroupTitle($title){
		$regexp = '/^[0-9]{3}[\p{Cyrillic}]{1}$/u';

		if(!preg_match($regexp, $title)) return false;

		return true;
	}
}
