<?php
namespace app\models;

use system\Model;

class Feed extends Model {
	static $table_name = 'feed';

	public static function toWrapUp($author_id, $type, $data){
		return [
			'created_at' => time(),
			'author_id' => $author_id,
			'type' => $type,
			'data' => serialize($data)
		];
	}

	public static function convertFeedToView($feed){
		$type = self::getType($feed['type']);
		$data = unserialize($feed['data']);
		$values = [];

		foreach($type['values'] as $value){
			$values[] = $data[$value];
		}

		$view = sprintf($type['view'], ...$values);

		return [
			'id' => $feed['id'],
			'date' => convert_date($feed['created_at']),
			'view' => $view
		];
	}

	private static function getType($type){
		$types = [
			1 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил нового модератора: <a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d)',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'moderator_id',
					'moderator_login',
					'moderator_id'
				]
			], 2 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил модератора: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'moderator_login'
				]
			], 3 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) отредактировал модератора: <a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d)',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'moderator_id',
					'moderator_login',
					'moderator_id'
				]
			], 4 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил группу: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'g_title'
				]
			], 5 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил группу: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'g_title'
				]
			], 6 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) изменил название группы <b>%s</b> на: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'old_g_title',
					'g_title'
				]
			], 7 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил предмет: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					's_title'
				]
			], 8 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) изменил название предмета <b>%s</b> на: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'old_s_title',
					's_title'
				]
			], 9 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил предмет: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					's_title'
				]
			], 10 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил преподавателя: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					't_title'
				]
			], 11 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) изменил имя преподавателя <b>%s</b> на: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'old_t_title',
					't_title'
				]
			], 12 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил преподавателя: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					't_title'
				]
			], 13 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил аудиторию: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'p_title'
				]
			], 14 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) изменил название аудитории <b>%s</b> на: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'old_p_title',
					'p_title'
				]
			], 15 => [
				'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил аудиторию: <b>%s</b>',
				'values' => [
					'author_id',
					'author_login',
					'author_id',
					'p_title'
				]
			], 16 => [
                'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) добавил запись в расписание: <b>%s</b>',
                'values' => [
                    'author_id',
                    'author_login',
                    'author_id',
                    'row_title'
                ]
            ], 17 => [
                'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) удалил запись из расписания: <b>%s</b>',
                'values' => [
                    'author_id',
                    'author_login',
                    'author_id',
                    'row_title'
                ]
            ], 18 => [
                'view' => '<a href="'.generateUrl(['users', 'view']).'&id=%d">%s</a> (ID: %d) отредактировал запись в расписании: <b>%s</b>',
                'values' => [
                    'author_id',
                    'author_login',
                    'author_id',
                    'row_title'
                ]
            ],
		];

		return $types[$type];
	}
}
