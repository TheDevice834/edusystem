<?php
namespace app\models;

use system\Model;

class Schedule extends Model {
	static $table_name = 'schedule';

	public static function dayToView($day = false){
	    $days = [
	        1 => 'Понедельник',
	        2 => 'Вторик',
	        3 => 'Среда',
	        4 => 'Четверг',
	        5 => 'Пятница',
	        6 => 'Суббота'
        ];

	    if($day != false) return $days[$day];
	    else return $days;
    }

    public static function sortRowsToView($rows){
	    $schedule = [];

        foreach($rows as $row){
            $group = Group::load($row['group_id']);
            $schedule[$row['day']]['rows'][] = [
                'id' => $row['id'],
                'group' => $group,
                'group_title' => $group['title'],
                'subject' => Subject::load($row['subject_id']),
                'teacher' => Teacher::load($row['teacher_id']),
                'place' => Place::load($row['place_id']),
                'row' => $row
            ];
        }

        foreach($schedule as $day => $value){
            $schedule[$day]['groups'] = [];
            $schedule[$day]['view'] = [];

            foreach($value['rows'] as $key => $row){
                $schedule[$day]['view'][$row['row']['time']][] = $row;

                if(!in_array($row['group_title'], $schedule[$day]['groups'])){
                    $schedule[$day]['groups'][] = $row['group_title'];
                }
            }

            $max_count = 0;
            foreach($schedule[$day]['view'] as $time => $row){
                if(count($schedule[$day]['view'][$time]) > $max_count) $max_count = count($schedule[$day]['view'][$time]);
                $schedule[$day]['view'][$time] = array_sort($row, 'group_title');
            }
            foreach($schedule[$day]['view'] as $time => $row) {
                for($i = 0; $i <= $max_count - 1; $i++){
                    if(!isset($row[$i])) $schedule[$day]['view'][$time][$i] = [];
                }
            }

            sort($schedule[$day]['groups']);
        }

        arsort($schedule);

        return $schedule;
    }

    public static function sortRowsToPanelView($rows){
        $schedule = [];

        foreach($rows as $key => $row){
            $schedule[$row['day']][] = [
                'id' => $row['id'],
                'group' => Group::load($row['group_id']),
                'group_title' => Group::load($row['group_id'])['title'],
                'subject' => Subject::load($row['subject_id']),
                'teacher' => Teacher::load($row['teacher_id']),
                'place' => Place::load($row['place_id']),
                'time' => $row['time'],
                'day' => self::dayToView($row['day']),
            ];
        }

        foreach($schedule as $key => $value){
            $schedule[$key] = array_sort($value, 'time');
            $schedule[$key] = array_sort($schedule[$key], 'group');
        }

        arsort($schedule);

        return $schedule;
    }
}
