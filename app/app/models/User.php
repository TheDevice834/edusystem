<?php
namespace app\models;

use system\Model;

class User extends Model {
	static $table_name = 'users';

	public static function checkLogin($login){
		$regexp = '~^[a-zA-Z0-9_-]+$~';
		$max_length = 24;
		$min_length = 4;

		if(!preg_match($regexp, $login)) return false;
		if(strlen($login) < $min_length || strlen($login) > $max_length) return false;

		return true;
	}

	public static function checkPassword($password){
		$max_length = 64;
		$min_length = 6;

		if(strlen($password) < $min_length || strlen($password) > $max_length) return false;

		return true;
	}
}
