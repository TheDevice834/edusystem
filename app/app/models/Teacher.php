<?php
namespace app\models;

use system\Model;

class Teacher extends Model {
	static $table_name = 'teachers';

	public static function checkTeacherTitle($title){
		$regexp = '/^[\p{Cyrillic}]+ [\p{Cyrillic}]{1}.[\p{Cyrillic}]{1}.$/u';

		$max_length = 64;
		$min_length = 2;

		if(!preg_match($regexp, $title)) return false;
		if(strlen($title) < $min_length || strlen($title) > $max_length) return false;

		return true;
	}
}
