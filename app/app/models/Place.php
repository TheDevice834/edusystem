<?php
namespace app\models;

use system\Model;

class Place extends Model {
	static $table_name = 'places';

	public static function checkPlaceTitle($title){
		$max_length = 64;
		$min_length = 2;

		if(strlen($title) < $min_length || strlen($title) > $max_length) return false;

		return true;
	}
}
