<?php
session_start();

define('ROOT_PATH', dirname(__DIR__));
define('PUBLIC_PATH', ROOT_PATH.'/public');
define('SYSTEM_PATH', ROOT_PATH.'/system');
define('APP_PATH', ROOT_PATH.'/app');
define('VENDOR_PATH', ROOT_PATH.'/vendor');

define('DB_DSN', 'mysql:host=db;dbname=edusystem');
define('DB_USER', 'root');
define('DB_PASSWORD', $_ENV['MYSQL_ROOT_PASSWORD']);

define('APP_NAME', 'EduSystem');
define('APP_VERSION', '0.2.2.2');
define('APP_NUMBER', '9');

require SYSTEM_PATH.'/functions.php';
require VENDOR_PATH.'/autoload.php';

spl_autoload_register(function($class_name){
	$class_path = str_replace('\\', '/', $class_name);
	$class_path = ROOT_PATH.'/'.$class_path.'.php';

	if(file_exists($class_path)) require $class_path;
});

$app = new system\App();
$app->run();
