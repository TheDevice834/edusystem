function ajaxWithAlert(url, data, callback){
	var request = $.ajax({
		method: 'POST',
		url: url,
		data: data,
		dataType: 'json'
	});

	request.done(function(data){
		if(data.type == 'error'){
			Swal.fire({
				type: 'error',
				html: data.message,
				timer: 3000
			});
		} else if(data.type == 'success'){
			callback(data);
		} else {
			Swal.fire({
				type: 'error',
				html: 'Что-то пошло не так...',
			});
		}
	});
	request.fail(function(data){
		Swal.fire({
			type: 'error',
			text: 'Что-то пошло не так...',
		});
	});
}

function deleteAjaxWithAlert(alert_text, delete_url, remove_id){
	Swal.fire({
		html: alert_text,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#d33',
		cancelButtonText: 'Закрыть',
		confirmButtonText: 'Удалить'
	}).then((result) => {
		if(result.value){
			ajaxWithAlert(delete_url, {}, function(data){
				$('#' + remove_id).remove();

				Swal.fire({
					type: 'success',
					html: data.message,
					timer: 3000
				});
			});
		}
	});
}
