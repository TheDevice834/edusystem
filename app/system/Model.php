<?php
namespace system;

use R;

class Model {
	public function __construct(){
		initDb();
	}

	public static function add($parameters){
		initDb();
		$model = R::dispense(static::$table_name);

		foreach($parameters as $key => $value){
			$model->$key = $value;
		}

    $result = self::load(R::store($model));
    R::close();

    return $result;
	}

	public static function findOne($parameters, $parameters_arr = []){
		initDb();
    $result = R::findOne(static::$table_name, $parameters, $parameters_arr);
    R::close();

    return $result;
	}

	public static function findAll($parameters, $parameters_arr = []){
		initDb();
    $result = R::findAll(static::$table_name, $parameters, $parameters_arr);
    R::close();

    return $result;
	}

	public static function count(){
		initDb();
    $result = R::count(static::$table_name);
    R::close();

    return $result;
	}

	public static function load($id){
		initDb();
    $result = R::load(static::$table_name, $id);
    R::close();

    return $result;
	}

	public static function store($model){
		initDb();
    $result = self::load(R::store($model));
    R::close();

    return $result;
	}

	public static function trash($model){
		initDb();
    $result = R::trash($model);
    R::close();

    return $result;
	}
}
