<?php
namespace system;

class Router {
	private $route;
	private $controller_key_name;
	private $method_key_name;

	public function __construct($route){
		$this->route = $route;

		$this->controller_key_name = 'section';
		$this->method_key_name = 'action';
	}

	public function toFind(){
		if(!isset($this->route[$this->controller_key_name])) $this->route[$this->controller_key_name] = 'main';
		if(!isset($this->route[$this->method_key_name])) $this->route[$this->method_key_name] = 'index';

		$controller_name = $this->route[$this->controller_key_name];
		$method_name = $this->route[$this->method_key_name];

		if(!$controller = $this->findController($controller_name)) return not_found();
		if(!$this->findMethod($controller, $method_name)) return not_found();

		$controller->$method_name();
	}

	private function findController($controller_name){
		$controller_namespace = 'app\\controllers\\'.ucfirst($controller_name);
		$controller_path = APP_PATH.'/controllers/'.ucfirst($controller_name).'.php';

		if(file_exists($controller_path)){
			$controller = new $controller_namespace();

			return $controller;
		}

		return false;
	}

	private function findMethod($class, $method_name){
		if(method_exists($class, $method_name)) return true;
		return false;
	}
}