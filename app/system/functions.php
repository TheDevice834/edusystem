<?php
function not_found(){
	echo 'Page not found';
	http_response_code(404);
	die();
}

function redirect($url = '', $is_back = true){
	if($url == '' && isset($_GET['back']) && $is_back) $url = $_GET['back'];
	else if($url == '') $url = $_SERVER['REQUEST_URI'];


	header('Location: '.$url);
	http_response_code(301);
	die();
}

function ajax($response){
	echo json_encode($response);
	die();
}

function convert_date($timestamp){
	$converted = date('d.m.Y в H:i', $timestamp);
    $current_date = time();

    if(($current_date - $timestamp) < 3600){
        $difference = $current_date - $timestamp;
        $min = round($difference / 60);

        if($min == 0) $converted = 'только что';
        else $converted = $min.' минут назад';
	} elseif(date('d.m.Y', $current_date) == date('d.m.Y', $timestamp)){
        $converted = 'сегодня в '.date('H:i', $timestamp);
	} elseif(date('m.Y', $current_date) == date('m.Y', $timestamp) && (intval(date('d', $current_date)) - 1) == intval(date('d', $timestamp))){
        $converted = 'вчера в '.date('H:i', $timestamp);
	} else {
        $months = [
            1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря'
        ];

		if(date('Y', $current_date) == date('Y', $timestamp)){
			$converted = date('d', $timestamp).' '.$months[intval(date('n', $timestamp))].' в '.date('H:i', $timestamp);
		} else {
			$converted = date('d', $timestamp).' '.$months[intval(date('n', $timestamp))].' '.date('Y', $timestamp).' в '.date('H:i', $timestamp);
		}
	}

    return $converted;
}

function get_uri(){
	return $_SERVER['REQUEST_URI'];
}

function check_is_not_auth(){
	if(is_auth()) redirect('/');
	return true;
}

function check_is_auth($is_authed = false){
	if(!is_auth()){
		$back_uri = get_uri();
		$route = generateUrl(['auth', 'signin', 'back' => $back_uri]);

		redirect($route);
	}

	return true;
}

function generateUrl($params){
	if(isset($params[1])){
		$action = $params[1];
		unset($params[1]);
		$params = ['action' => $action] + $params;
	}

	if(isset($params[0])){
		$section = $params[0];
		unset($params[0]);
		$params = ['section' => $section] + $params;
	}

	return '/index.php?'.http_build_query($params);
}

function initDb(){
	require_once SYSTEM_PATH.'/classes/rb-mysql.php';
	if(!R::testConnection()) R::setup(DB_DSN, DB_USER, DB_PASSWORD);
}

function add_alert($color, $message){
	$_SESSION['alerts'][] = [
		'color' => $color,
		'message' => $message
	];

	return true;
}

function is_auth(){
	return isset($_SESSION['user']);
}

function get_user(){
	return $_SESSION['user'];
}

function alerts_exists(){
	if(isset($_SESSION['alerts'])) return true;
	return false;
}

function get_alerts(){
	if(!isset($_SESSION['alerts'])) return false;

	$alerts = $_SESSION['alerts'];
	unset($_SESSION['alerts']);

	return $alerts;
}

function array_sort($array, $on, $order = SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}