<?php
namespace system;

class Controller {
	protected $view;
	protected $request;
	protected $regexp;

	public function __construct(){
		$this->loadControllerView();
		$this->loadControllerRequest();
	}

	private function loadControllerView(){
		$this->view = new View();
	}

	private function loadControllerRequest(){
		$this->request = [
			'METHOD' => $_SERVER['REQUEST_METHOD'],
			'GET' => $_GET,
			'POST' => $_POST
		];
	}
}
