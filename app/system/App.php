<?php
namespace system;

class App {
	private $route;
	private $router;

	public function __construct(){
		$this->route = $_GET;

		$router = new Router($this->route);
		$this->router = $router;
	}

	public function run(){
		$this->router->toFind();
	}
}