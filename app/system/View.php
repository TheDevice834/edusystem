<?php
namespace system;

class View {
	private $twig;

	public function __construct(){
		$loader = new \Twig_Loader_Filesystem(APP_PATH.'/views');
		$twig = new \Twig_Environment($loader);
		$this->twig = $twig;

		$this->addFunctions();
	}

	public function render($filename, $variables = []){
		echo $this->twig->render($filename, $variables);
	}

	private function addFunctions(){
		$this->twig->addFunction(new \Twig_Function('route', function($params = null){
            echo generateUrl($params);
        }));

		$this->twig->addFunction(new \Twig_Function('is_route', function($params = null){
			if(isset($params[0]) && !isset($params[1]) && $_GET['section'] == $params[0]) return true;
            elseif($_GET['section'] == $params[0] && $_GET['action'] == $params[1]) return true;
			else return false;
        }));

		$this->twig->addFunction(new \Twig_Function('alerts_exists', function($params = null){
            return alerts_exists();
        }));

		$this->twig->addFunction(new \Twig_Function('get_alerts', function($params = null){
            return get_alerts();
        }));

		$this->twig->addFunction(new \Twig_Function('is_auth', function($params = null){
            return is_auth();
        }));

		$this->twig->addFunction(new \Twig_Function('get_user', function($params = null){
            return get_user();
        }));

		$this->twig->addFunction(new \Twig_Function('get_uri', function($params = null){
            return get_uri();
        }));

		$this->twig->addFunction(new \Twig_Function('convert_date', function($params = null){
            return convert_date($params);
        }));

		$this->twig->addFunction(new \Twig_Function('constant', function($params = null){
            return constant($params);
        }));
	}
}
